import React, {useState} from 'react';
import {SafeAreaView, StyleSheet, ScrollView} from 'react-native';
import {Button, ListItem} from '@rneui/base';
import UploadFile from '../component/UploadFile';
import SelectView from '../component/SelectView';
import InputView from '../component/InputView';
import MintDuration from '../component/MintDuration';
import SelectButton from '../component/SelectButton';
import CollectionSelect from '../component/CollectionSelect';
import CreateCollection from '../createCollection';
import CollectionListView from '../component/CollectionListView';
import {CollectionOps} from '../component/CollectionView';

function CreateNFT(): React.JSX.Element {
  const [isVisible, setIsVisible] = useState(false);
  const [mintDuration, setMintDuration] = useState({
    title: '1 Week',
    duration: 60 * 60 * 24 * 7,
  });
  const [price, setPrice] = useState(0);
  const [mintStartIndex, setMintStartIndex] = useState(0);
  const [editionSizeIndex, setEditionSizeIndex] = useState(0);
  const [editionSize, setEditionSize] = useState(1000);
  const [createCollection, setCreateCollection] = useState(false);
  const [collectionListShow, setCollectionListShow] = useState(false);
  const [currentCollection, setCurrentCollection] = useState({
    title: '',
    network: '',
    collectionUrl: '',
  });

  return (
    <SafeAreaView style={styles.contentView}>
      <ScrollView style={styles.scrollerContain}>
        <ListItem containerStyle={styles.mainColor}>
          <ListItem.Content>
            <ListItem.Title style={styles.mainTitle}>
              Upload imageination
            </ListItem.Title>
            <UploadFile />
            <ListItem.Subtitle style={styles.subTitle}>
              Upload image, video,audio,PDF or GLB
            </ListItem.Subtitle>
          </ListItem.Content>
        </ListItem>
        <ListItem containerStyle={styles.mainColor}>
          <ListItem.Content>
            <ListItem.Title style={styles.mainTitle}>Collection</ListItem.Title>
            <CollectionSelect
              collection={currentCollection}
              onPress={() => {
                setCollectionListShow(true);
              }}
              onButtonClick={() => {
                setCreateCollection(true);
              }}
            />
          </ListItem.Content>
        </ListItem>
        <ListItem containerStyle={styles.mainColor}>
          <ListItem.Content>
            <ListItem.Title style={styles.mainTitle}>Price</ListItem.Title>
            <InputView
              placeholderText="0"
              keyboardType="number-pad"
              onChangeText={(text: string) => {
                setPrice(Number(text));
              }}
            />
            <ListItem.Subtitle style={styles.subTitle}>
              Set your price to 0 ETH to earn Creator Rewards
            </ListItem.Subtitle>
          </ListItem.Content>
        </ListItem>
        <ListItem containerStyle={styles.mainColor}>
          <ListItem.Content>
            <ListItem.Title style={styles.mainTitle}>
              Mint duration
            </ListItem.Title>
            <SelectView
              defaultText={mintDuration.title}
              onPress={() => {
                setIsVisible(true);
              }}
            />
          </ListItem.Content>
        </ListItem>
        <ListItem containerStyle={styles.mainColor}>
          <ListItem.Content>
            <ListItem.Title style={styles.mainTitle}>Mint start</ListItem.Title>
            <SelectButton
              selectIndex={mintStartIndex}
              buttonsTitle={['Now', 'Feature']}
              onPress={(index: number) => {
                setMintStartIndex(index);
              }}
            />
          </ListItem.Content>
        </ListItem>
        <ListItem containerStyle={styles.editionSize}>
          <ListItem.Content>
            <ListItem.Title style={styles.mainTitle}>
              Edition size
            </ListItem.Title>
            <SelectButton
              selectIndex={editionSizeIndex}
              buttonsTitle={['Open', 'Limited']}
              onPress={(index: number) => {
                setEditionSizeIndex(index);
                if (index === 0) {
                  setEditionSize(1000);
                }
              }}
            />
          </ListItem.Content>
        </ListItem>
        {editionSizeIndex === 1 ? (
          <ListItem containerStyle={styles.editionSize}>
            <ListItem.Content>
              <InputView
                placeholderText="1"
                keyboardType="number-pad"
                text={editionSize.toString()}
                onChangeText={(text: string) => {
                  setEditionSize(Number(text));
                }}
              />
            </ListItem.Content>
          </ListItem>
        ) : null}
        <Button
          title="Done"
          buttonStyle={styles.bottomButtonStyle}
          containerStyle={styles.buttomContainerStyle}
          titleStyle={styles.buttomTitleStyle}
        />
      </ScrollView>
      {isVisible ? (
        <MintDuration
          isVisible={isVisible}
          currentDuration={mintDuration.title}
          onClick={(title: string, duration: number) => {
            setMintDuration(prevDuration => ({
              ...prevDuration,
              title: title,
              duration: duration,
            }));
            setIsVisible(false);
          }}
        />
      ) : null}
      {createCollection ? (
        <CreateCollection
          isVisible={createCollection}
          onbackgroundPress={() => setCreateCollection(false)}
          onSavePress={() => setCreateCollection(false)}
        />
      ) : null}
      {collectionListShow ? (
        <CollectionListView
          isVisible={collectionListShow}
          onClick={(collection?: CollectionOps) => {
            console.log(collection?.title);
            setCurrentCollection(prevDuration => ({
              ...prevDuration,
              title: collection?.title || '',
              network: collection?.network || '',
              collectionUrl: collection?.collectionUrl || '',
            }));
            setCollectionListShow(false);
          }}
          onCreateClick={() => {
            setCurrentCollection(prevDuration => ({
              ...prevDuration,
              title: '',
              network: '',
              collectionUrl: '',
            }));
            setCollectionListShow(false);
            setCreateCollection(true);
          }}
        />
      ) : null}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  contentView: {
    flex: 1,
    backgroundColor: '#0A0A0A',
  },
  scrollerContain: {
    flex: 1,
  },
  mainColor: {
    backgroundColor: '#0A0A0A',
    paddingTop: 0,
  },
  editionSize: {
    backgroundColor: '#0A0A0A',
    paddingTop: 0,
    paddingBottom: 0,
  },
  mainTitle: {
    color: '#FEFEFE',
    fontWeight: 'bold',
  },
  subTitle: {
    color: '#A1A1A1',
  },
  bottomButtonStyle: {
    backgroundColor: '#FFF345',
    borderRadius: 36,
    width: '100%',
    height: 56,
  },
  buttomContainerStyle: {
    flex: 1,
    marginHorizontal: 15,
    marginVertical: 15,
  },
  buttomTitleStyle: {
    color: 'black',
    fontWeight: 'bold',
  },
});
export default CreateNFT;
