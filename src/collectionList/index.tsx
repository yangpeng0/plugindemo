import React from 'react';
import {SafeAreaView, View, StyleSheet} from 'react-native';
import {Text} from '@rneui/themed';
import {Button} from '@rneui/base';

function CollectionList(): React.JSX.Element {
  return (
    <SafeAreaView style={styles.contentView}>
      <View style={styles.remindContainer}>
        <Text h4 h4Style={styles.remindTitle}>
          No NFT collection URL yet?
        </Text>
        <Button
          title="Create now"
          type="clear"
          titleStyle={styles.createButton}
          onPress={() => console.log('jump createCollection')}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  contentView: {
    flex: 1,
    backgroundColor: '#0A0A0A',
  },
  remindTitle: {
    fontWeight: '400',
    fontSize: 15,
    color: '#FEFEFE',
  },
  remindContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginVertical: 20,
  },
  createButton: {
    color: '#FFF345',
  },
});
export default CollectionList;
