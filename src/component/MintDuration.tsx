/* eslint-disable react-native/no-inline-styles */
import {BottomSheet, ListItem} from '@rneui/base';
import React from 'react';

interface MintDurationOps {
  onClick?: (title: string, duration: number) => void;
  isVisible?: boolean;
  currentDuration?: string;
}

const durationList = [
  {title: '1 Hour', duration: 60 * 60},
  {
    title: '4 Hours',
    duration: 60 * 60 * 4,
  },
  {
    title: '24 Hour',
    duration: 60 * 60 * 24,
  },
  {
    title: '3 Days',
    duration: 60 * 60 * 24 * 3,
  },
  {
    title: '1 Week',
    duration: 60 * 60 * 24 * 7,
  },
  {
    title: '1 Month',
    duration: 60 * 60 * 24 * 30,
  },
  {
    title: '3 Months',
    duration: 60 * 60 * 24 * 30 * 3,
  },
  {
    title: '6 Months',
    duration: 60 * 60 * 24 * 30 * 6,
  },
  {title: 'Open', duration: 0},
];

const MintDuration: React.FC<MintDurationOps> = props => {
  const {isVisible, currentDuration, onClick} = props;

  const onPress = (index: number) => {
    const selectDuration = durationList[index];
    onClick && onClick(selectDuration.title, selectDuration.duration);
  };
  return (
    <BottomSheet
      modalProps={{}}
      isVisible={isVisible}
      scrollViewProps={{scrollEnabled: false}}
      backdropStyle={{backgroundColor: '#0A0A0ACC'}}>
      {durationList.map((l, i) => (
        <ListItem
          key={i}
          onPress={() => onPress(i)}
          containerStyle={{backgroundColor: '#252525'}}>
          <ListItem.Content>
            <ListItem.Title
              style={
                currentDuration === l.title
                  ? {color: '#FFF345'}
                  : {color: '#FEFEFE'}
              }>
              {l.title}
            </ListItem.Title>
          </ListItem.Content>
        </ListItem>
      ))}
    </BottomSheet>
  );
};

export default MintDuration;
