/* eslint-disable react-native/no-inline-styles */
import {Input} from '@rneui/base';
import React from 'react';
import {View, StyleSheet, KeyboardTypeOptions} from 'react-native';

interface InputViewOps {
  onChangeText?: (text: string) => void;
  placeholderText?: string;
  text?: string;
  keyboardType?: KeyboardTypeOptions;
  numberOfLines?: number;
  multiline?: boolean;
  backgroundColor?: string;
}

const InputView: React.FC<InputViewOps> = props => {
  const {
    placeholderText = 'please input text',
    text,
    keyboardType,
    onChangeText,
    numberOfLines = 1,
    multiline = false,
    backgroundColor = '#252525',
  } = props;
  return (
    <View style={[styles.contentView, {backgroundColor: backgroundColor}]}>
      <Input
        containerStyle={{height: 50}}
        inputContainerStyle={{
          borderBottomWidth: 0,
        }}
        defaultValue={text}
        keyboardType={keyboardType}
        inputStyle={styles.inputStyle}
        placeholder={placeholderText}
        numberOfLines={numberOfLines}
        multiline={multiline}
        onChangeText={value => {
          onChangeText && onChangeText(value);
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  contentView: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    width: '100%',
    marginHorizontal: 0,
    marginVertical: 10,
    borderRadius: 10,
  },
  inputStyle: {
    color: 'white',
    height: 50,
  },
});
export default InputView;
