/* eslint-disable react-native/no-inline-styles */
import {Button} from '@rneui/base';
import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';
import CollectionView, {CollectionOps} from './CollectionView';

interface CollectionSelectOps {
  onPress?: () => void; //整行view点击
  onButtonClick?: () => void; //button按钮点击
  collection?: CollectionOps;
}

const CollectionSelect: React.FC<CollectionSelectOps> = props => {
  const {onPress, onButtonClick, collection} = props;
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.contentView}>
        {collection?.title ? (
          <View style={{marginLeft: 10}}>
            <CollectionView
              title={collection?.title}
              network={collection?.network}
              collectionUrl={collection?.collectionUrl}
            />
          </View>
        ) : (
          <Button
            title="Create collection"
            iconContainerStyle={{marginLeft: 5}}
            titleStyle={{color: '#FEFEFE', fontSize: 15}}
            buttonStyle={{
              backgroundColor: '#0A0A0A',
              borderColor: 'white',
              borderWidth: 1,
              borderRadius: 10,
            }}
            containerStyle={{
              width: 140,
              marginHorizontal: 10,
              marginVertical: 10,
            }}
            onPress={onButtonClick}
          />
        )}
        <Image
          source={require('../assets/arrow.png')}
          style={styles.imageContainer}
        />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  contentView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#252525',
    height: 60,
    width: '100%',
    marginHorizontal: 0,
    marginVertical: 10,
    borderRadius: 10,
  },
  imageContainer: {
    width: 15,
    height: 15,
    margin: 15,
  },
});
export default CollectionSelect;
