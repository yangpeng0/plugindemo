/* eslint-disable react-native/no-inline-styles */
import {ButtonGroup} from '@rneui/base';
import React from 'react';
import {StyleSheet} from 'react-native';

interface SelectButtonOps {
  onPress?: (index: number) => void;
  buttonsTitle: Array<string>;
  selectIndex?: number;
}

const SelectButton: React.FC<SelectButtonOps> = props => {
  const {onPress, selectIndex, buttonsTitle} = props;
  return (
    <ButtonGroup
      buttons={buttonsTitle}
      selectedIndex={selectIndex}
      onPress={value => {
        onPress && onPress(value);
      }}
      containerStyle={styles.contentView}
      selectedButtonStyle={{backgroundColor: '#252525'}}
      textStyle={{color: '#A1A1A1', fontSize: 15}}
      selectedTextStyle={{color: '#FFF345', fontSize: 15}}
    />
  );
};

const styles = StyleSheet.create({
  contentView: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0A0A0A',
    height: 60,
    width: '100%',
    marginHorizontal: 0,
    marginVertical: 10,
    borderRadius: 10,
  },
});
export default SelectButton;
