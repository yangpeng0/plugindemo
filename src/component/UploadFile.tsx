import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Text} from '@rneui/themed';

const UploadFile: React.FC<any> = () => {
  return (
    <View style={styles.contentView}>
      <Text h4 h4Style={styles.remindTitle}>
        Draw with canvas or Upload
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  contentView: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#252525',
    height: 200,
    width: '100%',
    marginHorizontal: 0,
    marginVertical: 10,
    borderRadius: 10,
  },
  remindTitle: {
    fontSize: 15,
    color: '#FEFEFE',
  },
});
export default UploadFile;
