import {Image, Text} from '@rneui/base';
import React from 'react';

import {View, StyleSheet, TouchableOpacity} from 'react-native';

interface CollectionCoverOps {
  onPress?: () => void;
}

const CollectionCover: React.FC<CollectionCoverOps> = props => {
  const {onPress} = props;
  return (
    <TouchableOpacity onPress={onPress}>
      {/* <View style={styles.contentView}>
        <Image
          source={{
            uri: 'https://gips1.baidu.com/it/u=436886321,1020119268&fm=3028&app=3028&f=JPEG&fmt=auto',
          }}
          containerStyle={styles.imageContainer}
        />
        <View style={styles.titleContainer}>
          <Text h4 h4Style={styles.remindTitle}>
            Select a file
          </Text>
          <Text h4 h4Style={styles.remindTitle}>
            PNG, JPEG and GIF Supported
          </Text>
        </View>
      </View> */}
      <View style={styles.contentView}>
        <View style={styles.uploadContainer}>
          <Text
            h4
            h4Style={{
              color: '#FEFEFE',
            }}>
            +
          </Text>
        </View>
        <View style={styles.titleContainer}>
          <Text h4 h4Style={styles.remindTitle}>
            Select a file
          </Text>
          <Text h4 h4Style={styles.remindTitle}>
            PNG, JPEG and GIF Supported
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  contentView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0A0A0A',
    height: 70,
    width: '100%',
    marginVertical: 10,
    borderRadius: 10,
  },
  uploadContainer: {
    width: 50,
    height: 50,
    margin: 10,
    flex: 1,
    borderRadius: 5,
    borderColor: '#FEFEFE',
    borderWidth: 1,
    borderStyle: 'dashed',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    width: 50,
    height: 50,
    margin: 10,
    flex: 1,
    borderRadius: 5,
  },
  titleContainer: {
    flex: 5,
    height: 50,
    justifyContent: 'center',
  },
  remindTitle: {
    fontSize: 15,
    color: '#FEFEFE',
  },
});
export default CollectionCover;
