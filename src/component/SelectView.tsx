import {Text} from '@rneui/base';
import React from 'react';
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native';

interface SelectViewOps {
  onPress?: () => void;
  defaultText?: string;
  backgroundColor?: string;
}

const SelectView: React.FC<SelectViewOps> = props => {
  const {onPress, defaultText, backgroundColor = '#252525'} = props;
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.contentView, {backgroundColor: backgroundColor}]}>
        <Text h4 h4Style={styles.remindTitle}>
          {defaultText}
        </Text>
        <Image
          source={require('../assets/arrow.png')}
          style={styles.imageContainer}
        />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  contentView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 60,
    width: '100%',
    marginHorizontal: 0,
    marginVertical: 10,
    borderRadius: 10,
  },
  remindTitle: {
    fontSize: 15,
    color: '#FEFEFE',
    marginHorizontal: 10,
  },
  imageContainer: {
    width: 15,
    height: 15,
    margin: 15,
  },
  selectIcon: {
    marginHorizontal: 10,
  },
});
export default SelectView;
