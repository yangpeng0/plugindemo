/* eslint-disable react-native/no-inline-styles */
import {BottomSheet, ListItem} from '@rneui/base';
import React from 'react';
import {StyleSheet} from 'react-native';
import CollectionView, {CollectionOps} from './CollectionView';

interface CollectionListViewOps {
  onClick?: (collection?: CollectionOps) => void;
  isVisible?: boolean;
  onCreateClick?: () => void;
}

const collectionList = [
  {
    title: 'OSP1',
    network: 'Base',
    collectionUrl:
      'https://gips0.baidu.com/it/u=3602773692,1512483864&fm=3028&app=3028&f=JPEG&fmt=auto',
  },
  {
    title: 'OSP2',
    network: 'Base',
    collectionUrl:
      'https://gips1.baidu.com/it/u=436886321,1020119268&fm=3028&app=3028&f=JPEG&fmt=auto',
  },
  {
    title: 'OSP3',
    network: 'Base',
    collectionUrl:
      'https://gips2.baidu.com/it/u=195724436,3554684702&fm=3028&app=3028&f=JPEG&fmt=auto',
  },
];

const CollectionListView: React.FC<CollectionListViewOps> = props => {
  const {isVisible, onClick, onCreateClick} = props;

  const onPress = (index: number) => {
    const selectDuration = collectionList[index];
    onClick && onClick(selectDuration);
  };
  return (
    <BottomSheet
      modalProps={{}}
      isVisible={isVisible}
      scrollViewProps={{scrollEnabled: false}}
      backdropStyle={{backgroundColor: '#0A0A0ACC'}}>
      <ListItem
        bottomDivider
        containerStyle={{
          backgroundColor: '#252525',
        }}
        onPress={onCreateClick}>
        <ListItem.Content>
          <ListItem.Title style={styles.mainTitle}>
            Create collection
          </ListItem.Title>
        </ListItem.Content>
      </ListItem>
      {collectionList.map((l, i) => (
        <ListItem
          bottomDivider
          key={i}
          onPress={() => onPress(i)}
          containerStyle={{backgroundColor: '#252525', marginLeft: 0}}>
          <ListItem.Content>
            <CollectionView
              title={l.title}
              network={l.network}
              collectionUrl={l.collectionUrl}
            />
          </ListItem.Content>
        </ListItem>
      ))}
    </BottomSheet>
  );
};

const styles = StyleSheet.create({
  mainTitle: {
    color: '#FEFEFE',
    fontWeight: 'bold',
    marginVertical: 5,
  },
});
export default CollectionListView;
