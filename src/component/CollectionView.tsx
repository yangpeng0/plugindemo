import {Image, Text} from '@rneui/base';
import React from 'react';

import {View, StyleSheet} from 'react-native';

export interface CollectionOps {
  title?: string;
  network?: string;
  collectionUrl?: string;
}

const CollectionView: React.FC<CollectionOps> = props => {
  const {title, network, collectionUrl} = props;
  return (
    <View style={styles.contentView}>
      <Image
        source={{
          uri: collectionUrl,
        }}
        containerStyle={styles.imageContainer}
      />
      <View style={styles.titleContainer}>
        <Text h4 h4Style={styles.remindTitle}>
          {title}
        </Text>
        <Text h4 h4Style={styles.subTitle}>
          {network}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  contentView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  imageContainer: {
    width: 30,
    height: 30,
    borderRadius: 5,
  },
  titleContainer: {
    height: 30,
    justifyContent: 'center',
    marginLeft: 10,
  },
  remindTitle: {
    fontSize: 15,
    color: '#FEFEFE',
  },
  subTitle: {
    fontSize: 12,
    color: '#A1A1A1',
  },
});
export default CollectionView;
