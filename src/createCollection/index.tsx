/* eslint-disable react-native/no-inline-styles */
import {Button, ListItem, Overlay} from '@rneui/base';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import InputView from '../component/InputView';
import SelectView from '../component/SelectView';
import CollectionCover from '../component/CollectionCover';

interface CreateTokenOps {
  onSavePress?: () => void;
  onbackgroundPress?: () => void;
  isVisible: boolean;
}

const CreateCollection: React.FC<CreateTokenOps> = props => {
  const {onSavePress, onbackgroundPress, isVisible} = props;

  return (
    <Overlay
      isVisible={isVisible}
      onBackdropPress={onbackgroundPress}
      overlayStyle={styles.overlayStyle}
      backdropStyle={{backgroundColor: '#0A0A0ACC'}}>
      <ListItem containerStyle={styles.listTitle}>
        <ListItem.Content>
          <ListItem.Title style={styles.mainTitle}>
            Create collection
          </ListItem.Title>
          <ListItem.Subtitle style={styles.subTitle}>
            A collection is like a folder or an album that you can keep adding
            to in the future.
          </ListItem.Subtitle>
        </ListItem.Content>
      </ListItem>
      <ListItem containerStyle={styles.listTitle}>
        <ListItem.Content>
          <ListItem.Title style={styles.listItemTitle}>Title</ListItem.Title>
          <InputView
            placeholderText="Spring Summer"
            backgroundColor="#0A0A0A"
            onChangeText={(text: string) => {
              console.log(text);
            }}
          />
        </ListItem.Content>
      </ListItem>
      <ListItem containerStyle={styles.listTitle}>
        <ListItem.Content>
          <ListItem.Title style={styles.listItemTitle}>
            Description (optional)
          </ListItem.Title>
          <InputView
            placeholderText="Spring Summer Notes and Materials"
            backgroundColor="#0A0A0A"
            numberOfLines={2}
            multiline={true}
            onChangeText={(text: string) => {
              console.log(text);
            }}
          />
        </ListItem.Content>
      </ListItem>
      <ListItem containerStyle={styles.listTitle}>
        <ListItem.Content>
          <ListItem.Title style={styles.listItemTitle}>
            Collection cover
          </ListItem.Title>
          <CollectionCover />
        </ListItem.Content>
      </ListItem>
      <ListItem containerStyle={styles.listTitle}>
        <ListItem.Content>
          <ListItem.Title style={styles.listItemTitle}>NetWork</ListItem.Title>
          <SelectView defaultText="Base" backgroundColor="#0A0A0A" />
        </ListItem.Content>
      </ListItem>

      <View style={styles.bottomView}>
        <Button
          title="Cancel"
          buttonStyle={{
            backgroundColor: '#0A0A0A',
            borderRadius: 5,
            height: '100%',
          }}
          titleStyle={{fontWeight: 'bold', fontSize: 18, color: '#FEFEFE'}}
          containerStyle={styles.buttonContainer}
          onPress={onbackgroundPress}
        />
        <Button
          title="Save"
          buttonStyle={{
            backgroundColor: '#FFF345',
            borderRadius: 5,
            height: '100%',
          }}
          titleStyle={{fontWeight: 'bold', fontSize: 18, color: '#0A0A0A'}}
          containerStyle={styles.buttonContainer}
          onPress={onSavePress}
        />
      </View>
    </Overlay>
  );
};

const styles = StyleSheet.create({
  overlayStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#252525',
    marginHorizontal: 15,
    marginVertical: 10,
    borderRadius: 10,
  },

  listTitle: {
    backgroundColor: '#252525',
    paddingTop: 0,
    width: '100%',
    paddingHorizontal: 5,
  },

  mainTitle: {
    color: '#FEFEFE',
    fontWeight: 'bold',
    fontSize: 18,
  },

  listItemTitle: {
    color: '#FEFEFE',
    fontWeight: 'bold',
    fontSize: 16,
  },

  subTitle: {
    color: '#A1A1A1',
    marginTop: 10,
  },

  bottomView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonContainer: {
    flex: 1,
    margin: 5,
    height: 45,
  },
});
export default CreateCollection;
