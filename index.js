/**
 * @format
 */

import {AppRegistry} from 'react-native';
import CollectionList from './src/collectionList';
import CreateNFT from './src/createNFT';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => CreateNFT);
